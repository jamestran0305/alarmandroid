﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;

namespace XamarinAndroidAlarmDemo.BroadCast
{
    [BroadcastReceiver]
    public class AlarmNotificationReceiver : BroadcastReceiver
    {
        protected MediaPlayer player;
        private AudioManager audioManager;

        public void StartPlayer(String filePath, Context context)
        {
            if (player == null)
            {
                player = new MediaPlayer();
            }
            player.Reset();
            var descriptor = context.Assets.OpenFd(filePath);
            player.SetDataSource(descriptor.FileDescriptor, descriptor.StartOffset, descriptor.Length);
            player.Prepare();
            player.Start();
        }
        public override void OnReceive(Context context, Intent intent)
        {
            //Create intent for action 1 (STOP)
            var alarmReceiver = new AlarmNotificationReceiver();
            var actionIntent1 = new Intent();
            actionIntent1.SetAction("STOP");
            var pIntent1 = PendingIntent.GetBroadcast(context, 0, actionIntent1, PendingIntentFlags.CancelCurrent);


            string CHANNEL_ID = "my_channel_01";// The id of the channel. 
            string name = "Test";// The user-visible name of the channel.
            var importance = NotificationImportance.High;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance)
            {
                Description = "asdas"
            };
            mChannel.EnableVibration(true);
            mChannel.LockscreenVisibility = NotificationVisibility.Public;
            // Create a notification and set the notification channel.

            NotificationManager notificationManager = (NotificationManager)context.GetSystemService(Context.NotificationService);
            notificationManager.CreateNotificationChannel(mChannel);

            Notification notification = new Notification.Builder(context, CHANNEL_ID)
                        .SetAutoCancel(true)
                        .AddAction(Resource.Drawable.abc_ic_star_black_36dp, "ARCHIVE", pIntent1)
                        .SetSmallIcon(Resource.Mipmap.Icon)
                        .SetChannelId(CHANNEL_ID).Build();

            //var intentFilter = new IntentFilter();
            //intentFilter.AddAction("STOP");

            //context.RegisterReceiver(alarmReceiver, intentFilter);

            NotificationManager manager = (NotificationManager)context.GetSystemService(Context.NotificationService);
            manager.Notify(10001, notification);
            StartPlayer("perfect.mp3", context);
         
        }
    }
}
