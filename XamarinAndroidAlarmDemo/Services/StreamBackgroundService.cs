﻿
using System;

using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Media;
using Android.Net;
using Android.Net.Wifi;
using Android.OS;
using XamarinAndroidAlarmDemo.BroadCast;

namespace XamarinAndroidAlarmDemo.Services
{
    [Service]
    [IntentFilter(new[] {ActionPlay, ActionPause, ActionStop })]
    public class StreamBackgroundService : Service, AudioManager.IOnAudioFocusChangeListener
    {
        //Actions
        public const string ActionPlay = "com.xamarin.action.PLAY";
        public const string ActionPause = "com.xamarin.action.PAUSE";
        public const string ActionStop = "com.xamarin.action.STOP";

        private const string Mp3 = @"https://sample-videos.com/audio/mp3/crowd-cheering.mp3";

        private MediaPlayer player;
        private AudioManager audioManager;
        private WifiManager wifiManager;
        private WifiManager.WifiLock wifiLock;
        private bool paused;

        private AlarmManager alarmMgr;
        private PendingIntent alarmIntent;

        private const int NotificationId = 1000;

        /// <summary>
        /// On create simply detect some of our managers
        /// </summary>
        public override void OnCreate()
        {
            base.OnCreate();
            //Find our audio and notificaton managers
            audioManager = (AudioManager)GetSystemService(AudioService);
            wifiManager = (WifiManager)GetSystemService(WifiService);
        }

        IBinder binder;

        private void IntializePlayer()
        {
            player = new MediaPlayer();

            //Tell our player to sream music
            player.SetAudioStreamType(Stream.Music);

            //Wake mode will be partial to keep the CPU still running under lock screen
            player.SetWakeMode(ApplicationContext, WakeLockFlags.Partial);

            //When we have prepared the song start playback
            player.Prepared += (sender, args) => player.Start();

            //When we have reached the end of the song stop ourselves, however you could signal next track here.
            player.Completion += (sender, args) => Stop();

            player.Error += (sender, args) =>
            {
                //playback error
                Console.WriteLine("Error in playback resetting: " + args.What);
                Stop();//this will clean up and reset properly.
            };
        }


        public override StartCommandResult OnStartCommand(Android.Content.Intent intent, StartCommandFlags flags, int startId)
        {
            alarmMgr = (AlarmManager)this.GetSystemService(Context.AlarmService);
            Intent intent1 = new Intent(this, typeof(AlarmNotificationReceiver));
            alarmIntent = PendingIntent.GetBroadcast(this, 0, intent1, 0);

            // Set the alarm to start at approximately 2:00 p.m.
            Java.Util.Calendar calendar = Java.Util.Calendar.Instance;
            calendar.TimeInMillis = Java.Lang.JavaSystem.CurrentTimeMillis();
            calendar.Set(Java.Util.CalendarField.HourOfDay, 13);
            calendar.Set(Java.Util.CalendarField.Minute, 48);
            calendar.Set(Java.Util.CalendarField.Second, 0);

            // With setInexactRepeating(), you have to use one of the AlarmManager interval
            // constants--in this case, AlarmManager.INTERVAL_DAY.
            alarmMgr.SetInexactRepeating(AlarmType.RtcWakeup, calendar.TimeInMillis,
                    AlarmManager.IntervalDay, alarmIntent);

            // start your service logic here

            string CHANNEL_ID = "my_channel_01";// The id of the channel. 
            string name = "Test";// The user-visible name of the channel.
            var importance = NotificationImportance.High;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance)
            {
                Description = "asdas" 
            };
            mChannel.EnableVibration(true);
            // Create a notification and set the notification channel.

            NotificationManager notificationManager =
        (NotificationManager)GetSystemService(NotificationService);
            notificationManager.CreateNotificationChannel(mChannel);

            Notification notification = new Notification.Builder(this, CHANNEL_ID)
                        .SetContentIntent(BuildIntentToShowMainActivity())
                        .SetSmallIcon(Resource.Mipmap.Icon)
                        .SetChannelId(CHANNEL_ID)
                        .Build();
                        
            StartForeground(NotificationId, notification);

            ////StartForeground();
            //switch (intent.Action)
            //{
            //    case ActionPlay: Play(); break;
            //    case ActionStop: Stop(); break;
            //    case ActionPause: Pause(); break;
            //}

            // Return the correct StartCommandResult for the type of service you are building
            return StartCommandResult.NotSticky;
        }

        PendingIntent BuildIntentToShowMainActivity()
        {
            var notificationIntent = new Intent(this, typeof(MainActivity));
            notificationIntent.SetAction("MAIN_ACTIVITY");
            notificationIntent.SetFlags(ActivityFlags.SingleTop | ActivityFlags.ClearTask);
            notificationIntent.PutExtra("has_service_been_started", true);

            var pendingIntent = PendingIntent.GetActivity(this, 0, notificationIntent, PendingIntentFlags.UpdateCurrent);
            return pendingIntent;
        }


        private async void Play()
        {
            if (paused && player != null)
            {
                paused = false;
                //We are simply paused so just start again
                player.Start();
                //StartForeground();
                return;
            }

            if (player == null)
            {
                IntializePlayer();
            }

            if (player.IsPlaying)
                return;

            try
            {
                AssetFileDescriptor afd = Assets.OpenFd("perfect.mp3");

                await player.SetDataSourceAsync(afd.FileDescriptor, afd.StartOffset, afd.Length);

                var focusResult = audioManager.RequestAudioFocus(this, Stream.Music, AudioFocus.Gain);
                if (focusResult != AudioFocusRequest.Granted)
                {
                    //could not get audio focus
                    Console.WriteLine("Could not get audio focus");
                }

                player.PrepareAsync();
                //AquireWifiLock();
                //StartForeground();
            }
            catch (System.Exception ex)
            {
                //unable to start playback log error
                Console.WriteLine("Unable to start playback: " + ex);
            }
        }

        /// <summary>
        /// When we start on the foreground we will present a notification to the user
        /// When they press the notification it will take them to the main page so they can control the music
        /// </summary>
        //private void StartForeground()
        //{

        //    var pendingIntent = PendingIntent.GetActivity(ApplicationContext, 0,
        //                    new Intent(ApplicationContext, typeof(MainActivity)),
        //                    PendingIntentFlags.UpdateCurrent);

        //    var notification = new Notification
        //    {
        //        TickerText = new Java.Lang.String("Song started!"),
        //        Icon = Resource.Mipmap.Icon
        //    };
        //    notification.Flags |= NotificationFlags.OngoingEvent;
        //    notification.SetLatestEventInfo(ApplicationContext, "Xamarin Streaming",
        //                    "Playing music!", pendingIntent);
        //    StartForeground(NotificationId, notification);
        //}

        private void Pause()
        {
            if (player == null)
                return;

            if (player.IsPlaying)
                player.Pause();

            StopForeground(true);
            paused = true;
        }

        private void Stop()
        {
            if (player == null)
                return;

            if (player.IsPlaying)
                player.Stop();

            player.Reset();
            paused = false;
            StopForeground(true);
            ReleaseWifiLock();
        }

        /// <summary>
        /// Lock the wifi so we can still stream under lock screen
        /// </summary>
        private void AquireWifiLock()
        {
            if (wifiLock == null)
            {
                wifiLock = wifiManager.CreateWifiLock(WifiMode.Full, "xamarin_wifi_lock");
            }
            wifiLock.Acquire();
        }

        /// <summary>
        /// This will release the wifi lock if it is no longer needed
        /// </summary>
        private void ReleaseWifiLock()
        {
            if (wifiLock == null)
                return;

            wifiLock.Release();
            wifiLock = null;
        }

        /// <summary>
        /// Properly cleanup of your player by releasing resources
        /// </summary>
        public override void OnDestroy()
        {
            base.OnDestroy();
            if (player != null)
            {
                player.Release();
                player = null;
            }
            StopForeground(true);
        }

        /// <summary>
        /// For a good user experience we should account for when audio focus has changed.
        /// There is only 1 audio output there may be several media services trying to use it so
        /// we should act correctly based on this.  "duck" to be quiet and when we gain go full.
        /// All applications are encouraged to follow this, but are not enforced.
        /// </summary>
        /// <param name="focusChange"></param>
        public void OnAudioFocusChange(AudioFocus focusChange)
        {
            switch (focusChange)
            {
                case AudioFocus.Gain:
                    if (player == null)
                        IntializePlayer();

                    if (!player.IsPlaying)
                    {
                        player.Start();
                        paused = false;
                    }

                    player.SetVolume(1.0f, 1.0f);//Turn it up!
                    break;
                case AudioFocus.Loss:
                    //We have lost focus stop!
                    Stop();
                    break;
                case AudioFocus.LossTransient:
                    //We have lost focus for a short time, but likely to resume so pause
                    Pause();
                    break;
                case AudioFocus.LossTransientCanDuck:
                    //We have lost focus but should till play at a muted 10% volume
                    if (player.IsPlaying)
                        player.SetVolume(.1f, .1f);//turn it down!
                    break;

            }
        }

        public override IBinder OnBind(Intent intent)
        {
            binder = new StreamBackgroundServiceBinder(this);
            return binder;
        }
    }

    public class StreamBackgroundServiceBinder : Binder
    {
        readonly StreamBackgroundService service;

        public StreamBackgroundServiceBinder(StreamBackgroundService service)
        {
            this.service = service;
        }

        public StreamBackgroundService GetStreamBackgroundService()
        {
            return service;
        }
    }
}
