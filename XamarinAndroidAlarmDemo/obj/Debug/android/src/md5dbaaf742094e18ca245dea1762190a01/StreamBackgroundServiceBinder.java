package md5dbaaf742094e18ca245dea1762190a01;


public class StreamBackgroundServiceBinder
	extends android.os.Binder
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("XamarinAndroidAlarmDemo.Services.StreamBackgroundServiceBinder, XamarinAndroidAlarmDemo", StreamBackgroundServiceBinder.class, __md_methods);
	}


	public StreamBackgroundServiceBinder ()
	{
		super ();
		if (getClass () == StreamBackgroundServiceBinder.class)
			mono.android.TypeManager.Activate ("XamarinAndroidAlarmDemo.Services.StreamBackgroundServiceBinder, XamarinAndroidAlarmDemo", "", this, new java.lang.Object[] {  });
	}

	public StreamBackgroundServiceBinder (md5dbaaf742094e18ca245dea1762190a01.StreamBackgroundService p0)
	{
		super ();
		if (getClass () == StreamBackgroundServiceBinder.class)
			mono.android.TypeManager.Activate ("XamarinAndroidAlarmDemo.Services.StreamBackgroundServiceBinder, XamarinAndroidAlarmDemo", "XamarinAndroidAlarmDemo.Services.StreamBackgroundService, XamarinAndroidAlarmDemo", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
