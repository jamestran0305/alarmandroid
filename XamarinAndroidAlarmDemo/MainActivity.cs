﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using Android.Content;
using XamarinAndroidAlarmDemo.BroadCast;
using XamarinAndroidAlarmDemo.Services;

namespace XamarinAndroidAlarmDemo
{
    [Activity(Label = "XamarinAndroidAlarmDemo", MainLauncher = true, Icon = "@mipmap/icon", Theme = "@style/Theme.AppCompat.Light.NoActionBar")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            var rdiNotification = FindViewById<RadioButton>(Resource.Id.rdiNotification);
            var rdiToast = FindViewById<RadioButton>(Resource.Id.rdiToast);

            var btnOneTime = FindViewById<Button>(Resource.Id.btnOneTime);
            var btnRepeating = FindViewById<Button>(Resource.Id.btnRepeating);

            btnOneTime.Click += (sender, e) =>
            {
                if (rdiNotification.Checked)
                {
                    SetAlarm(true, false);
                }
                else
                {
                    SetAlarm(false, false);
                }
            };

            btnRepeating.Click += (sender, e) => 
            {
                if (rdiNotification.Checked)
                {
                    SetAlarm(true, false);
                }
                else
                {
                    SetAlarm(false, true);
                }
            };

            var play = FindViewById<Button>(Resource.Id.playButton);
            var pause = FindViewById<Button>(Resource.Id.pauseButton);
            var stop = FindViewById<Button>(Resource.Id.stopButton);

            play.Click += (sender, args) => SendAudioCommand(StreamBackgroundService.ActionPlay);
            pause.Click += (sender, args) => SendAudioCommand(StreamBackgroundService.ActionPause);
            stop.Click += (sender, args) => SendAudioCommand(StreamBackgroundService.ActionStop);

        }

        private void SendAudioCommand(string action)
        {
            Toast.MakeText(this, "Show", ToastLength.Short).Show();
            var intent = new Intent(this.BaseContext, typeof(StreamBackgroundService));
            intent.SetAction(StreamBackgroundService.ActionPlay);
            //StartService(intent);
            if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.O)
            {
                StartForegroundService(intent);
            }
            else
            {
                StartService(intent);
            }
        }

        private void SetAlarm(bool isNotification, bool isRepeating)
        {
            AlarmManager manager = (AlarmManager)GetSystemService(Context.AlarmService);
            Intent myIntent;
            PendingIntent pendingIntent;

            if (!isNotification)
            {
                myIntent = new Intent(this, typeof(AlarmToastReceiver));
                pendingIntent = PendingIntent.GetBroadcast(this, 0, myIntent, 0);
            }
            else
            {
                myIntent = new Intent(this, typeof(AlarmNotificationReceiver));
                pendingIntent = PendingIntent.GetBroadcast(this, 0, myIntent, 0);
            }

            if (!isRepeating)
            {
                Java.Util.Calendar calendar = Java.Util.Calendar.Instance;
                calendar.TimeInMillis = Java.Lang.JavaSystem.CurrentTimeMillis();
                calendar.Set(Java.Util.CalendarField.HourOfDay, 13);
                calendar.Set(Java.Util.CalendarField.Minute, 12);
                calendar.Set(Java.Util.CalendarField.Second, 01);

                // With setInexactRepeating(), you have to use one of the AlarmManager interval
                // constants--in this case, AlarmManager.INTERVAL_DAY.
                manager.SetInexactRepeating(AlarmType.RtcWakeup, calendar.TimeInMillis,
                        AlarmManager.IntervalDay, pendingIntent);
                //manager.Set(AlarmType.RtcWakeup, SystemClock.ElapsedRealtime() + 3000, pendingIntent);
            }
            else
            {
                manager.SetRepeating(AlarmType.RtcWakeup, SystemClock.ElapsedRealtime() + 3000, 60 * 1000, pendingIntent);
            }
        }
    }
}

